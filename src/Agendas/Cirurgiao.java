/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Agendas;

import java.io.IOException;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 *
 * @author a1906453
 */
public class Cirurgiao {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws RemoteException, AlreadyBoundException, IOException {
        Cirurgiao_Impl cirilo = new Cirurgiao_Impl();
        Registry registro = LocateRegistry.createRegistry(1098);
        registro.bind("Cirurgiao", cirilo);
    }
    
}
